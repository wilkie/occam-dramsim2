# DOCKER-VERSION 0.11.1
FROM occam/mjqxgzi--ovrhk3tuouwweyltmuwtembrgqwtanq-
ADD . /simulator
RUN apt-get update
RUN git clone git://github.com/dramninjasUMD/DRAMSim2.git /simulator/package
RUN cd /occam; echo 'pyenv local 3.3.0; python /simulator/launch.py' > execute.sh
RUN cd /simulator; /bin/bash -c 'source /occam/setup_python.sh; source /occam/setup_perl.sh; cd /simulator; bash /simulator/build.sh'
CMD ['/bin/bash']
